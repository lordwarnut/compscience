import tkinter as tk
from models import Booking, Bus


class FinishedFrame(tk.Frame):

    def __init__(self, parent, bus: Bus, booking: Booking, goback_callback, confirm_callback):
        # TODO: determine if seat cost include gst
        order_summery = f"""
        Summery of order for {booking.name}:
            Name               : {booking.name}
            Phone number       : {booking.phone_number}
            Bus Service Booked : {bus.get_destination_str()}
            Date               : {bus.get_date_str()}

            Seats bought : ${booking.seat}
            Seat Cost (${round(bus.SEAT_COST, 2)}ea) : ${round(booking.get_seat_cost(), 2)}

            Bunks bought : ${booking.bunk}
            Bunk Cost (${round(bus.BUNK_COST, 2)}ea) : ${round(booking.get_bunk_cost(), 2)}

            Subtotal: ${round(booking.get_seat_cost()/1.15 + booking.get_bunk_cost()/1.15, 2)}
              + GST : ${round((booking.get_seat_cost() + booking.get_bunk_cost())/1.15, 2)}
        -----------------------------
               Total: ${round(booking.get_seat_cost() + booking.get_bunk_cost(), 2)}
        """

        super().__init__(parent)
        self.parent = parent
        self.bus = bus
        self.booking = booking
        self.goback_callback = goback_callback
        self.confirm_callback = confirm_callback

        self.welcomelabel = tk.Label(self, text="Welcome", font=("Helvetica", 16))
        self.welcomelabel.grid(row=0, column=0, columnspan=2)

        self.abouttext = tk.Text(self, wrap=tk.WORD)
        self.abouttext.insert(tk.INSERT, order_summery)
        self.abouttext.grid(row=1, column=0, columnspan=2, sticky="news")
        self.abouttext.config(state=tk.DISABLED)

        self.back_button = tk.Button(self, text="Go Back", command=self.goback_button_command)
        self.confirm_button = tk.Button(self, text="Confirm", command=self.confirm_button_command)

        self.back_button.grid(row=2, column=0)
        self.confirm_button.grid(row=2, column=1)

        self.grid_rowconfigure(0, weight=0)
        self.grid_rowconfigure(1, minsize=10, weight=1)

    def goback_button_command(self):
        self.goback_callback(self.bus, self.booking)

    def confirm_button_command(self):
        self.confirm_callback(self.bus, self.booking)
