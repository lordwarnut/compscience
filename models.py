import datetime
import enum


class State(enum.Enum):
    NULL = 0
    friday = 1
    sunday = 2


class Booking:
    def __init__(self, name, phone_number, seat, bunk):
        self.name = name
        self.phone_number = phone_number
        self.seat = seat
        self.bunk = bunk

    def get_seat_cost(self):
        return Bus.SEAT_COST * self.seat

    def get_bunk_cost(self):
        return Bus.BUNK_COST * self.bunk


class Bus:
    MAX_SEAT = 20
    MAX_BUNK = 15
    SEAT_COST = 25
    BUNK_COST = 25

    def __init__(self, state, **kwargs):
        self.base_bunk = kwargs.get('bunk', 0)
        self.base_seat = kwargs.get('seat', 0)
        self.booking_list: [Booking] = []
        self.state = state

    @property
    def bunk(self) -> int:
        return self.base_bunk + sum(booking.bunk for booking in self.booking_list)

    @bunk.setter
    def bunk(self, val: int):
        self.base_bunk = val

    @property
    def seat(self) -> int:
        return self.base_seat + sum(booking.seat for booking in self.booking_list)

    @seat.setter
    def seat(self, val: int):
        self.base_seat = val

    def get_date_str(self):
        if self.state == State.friday:
            friday = datetime.date.today() + datetime.timedelta((4 - datetime.date.today().weekday()) % 7)
            return friday.strftime('%a %d-%b-%Y')
        elif self.state == State.sunday:
            sunday = datetime.date.today() + datetime.timedelta((5 - datetime.date.today().weekday()) % 7)
            return sunday.strftime('%a %d-%b-%Y')
        return "NULL"

    def get_destination_str(self):
        if self.state == State.friday:
            return 'Auckland'
        elif self.state == State.sunday:
            return 'Palmerston North'
        return "NULL"
