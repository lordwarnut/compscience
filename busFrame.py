import tkinter as tk
from models import Bus


class BusFrame(tk.Frame):
    bus_fstring = "Bus Service to {}"
    date_fstring = "Date : {}"
    seat_fstring = "Seats Available  : {}"
    bunk_fstring = "Bunks Available : {}"

    def __init__(self, parent):
        super().__init__(parent)

        self.parent = parent

        self.titlelabel = tk.Label(self, text=self.bus_fstring.format(""))
        self.titlelabel.grid(row=0, column=0, sticky='w')

        self.grid_rowconfigure(1, minsize=10)

        self.datelabel = tk.Label(self, text=self.date_fstring.format(""))
        self.datelabel.grid(row=2, column=0, sticky='w')

        self.seatlabel = tk.Label(self, text=self.seat_fstring.format(""))
        self.seatlabel.grid(row=3, column=0, sticky='w')

        self.bunklabel = tk.Label(self, text=self.bunk_fstring.format(""))
        self.bunklabel.grid(row=4, column=0, sticky='w')

        self.columnconfigure(0, minsize=200)

    def set_labels(self, bus: Bus):
        self.titlelabel['text'] = self.bus_fstring.format(bus.get_destination_str())
        self.datelabel['text'] = self.date_fstring.format(bus.get_date_str())
        self.seatlabel['text'] = self.seat_fstring.format(bus.seat)
        self.bunklabel['text'] = self.bunk_fstring.format(bus.bunk)