import tkinter as tk


class WelcomeFrame(tk.Frame):
    aboutstr = """
    This is a simple bus booking application for the Massey Overnighter developed for The Go Student Bus company

    There are two buses that run each week.
        -Friday : An overnight bus leaving Palmerston North for Auckland.
        -Sunday : An overnight bus leaving Auckland for Palmerston North.

    Each bus can only be booked on the monday preceding it or after.
    There are 15 bunk beds and 20 reclining seats

    The prices are as follows:
         - Reclining Seat : $25
         - Bunk Bed : $50

    """

    def __init__(self, parent):
        super().__init__(parent)

        self.welcomelabel = tk.Label(self, text="Welcome", font=("Helvetica", 16))
        self.welcomelabel.grid(row=0, column=0)

        self.abouttext = tk.Text(self, wrap=tk.WORD)
        self.abouttext.insert(tk.INSERT, self.aboutstr)
        self.abouttext.grid(row=1, column=0, sticky="news")
        self.abouttext.config(state=tk.DISABLED)

        self.grid_rowconfigure(0, weight=0)
        self.grid_rowconfigure(1, minsize=10, weight=1)