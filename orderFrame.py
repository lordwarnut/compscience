import tkinter as tk
from models import Booking, Bus
import tkinter.messagebox as messagebox

class OrderFrame(tk.Frame):

    def __init__(self, parent, bus: Bus, confirm_callback, cancel_callback):
        super().__init__(parent)
        self.bus = bus
        self.parent = parent
        self.confirm_callback = confirm_callback
        self.cancel_callback = cancel_callback

        self.order_label = tk.Label(self, text="Order Form", font=("Helvetica", 16))
        self.order_label.grid(row=0, column=0)

        self.entry_frame = tk.Frame(self)
        self.entry_form_init(bus)
        self.entry_frame.grid(row=1, column=0, sticky='news')

        self.order_now_button = tk.Button(self, text="Order Now!", command=self.order_confirm_command)
        self.order_now_button.grid(row=2, column=0)

        self.cancel_button = tk.Button(self, text="Cancel Booking", command=self.order_cancel_command)
        self.cancel_button.grid(row=3, column=0)

        self.columnconfigure(0, weight=1)

    def entry_form_init(self, bus: Bus):
        # TODO: Change entry colour validation
        # TODO: add num labels
        self.name_label = tk.Label(self.entry_frame, text='Name: ')
        self.numb_label = tk.Label(self.entry_frame, text='Phone Number: ')
        self.bdes_label = tk.Label(self.entry_frame, text='Bus Destination: ')
        self.bdat_label = tk.Label(self.entry_frame, text='Leaving on: ')
        self.seat_label = tk.Label(self.entry_frame, text='Number of Seats to book: ')
        self.bunk_label = tk.Label(self.entry_frame, text='Numbers of bunks to book: ')

        self.name_label.grid(row=0, column=0)
        self.numb_label.grid(row=1, column=0)
        self.bdes_label.grid(row=2, column=0)
        self.bdat_label.grid(row=3, column=0)
        self.seat_label.grid(row=4, column=0)
        self.bunk_label.grid(row=5, column=0)

        self.name_entry = tk.Entry(self.entry_frame, justify=tk.RIGHT, text='Name: ')
        self.numb_entry = tk.Entry(self.entry_frame, justify=tk.RIGHT, text='Phone Number: ')
        self.bdes_entry = tk.Entry(self.entry_frame, justify=tk.RIGHT, text='Bus Destination: ')
        self.bdat_entry = tk.Entry(self.entry_frame, justify=tk.RIGHT, text='Leaving on: ')
        self.seat_entry = tk.Entry(self.entry_frame, justify=tk.RIGHT, text='Number of Seats to book: ')
        self.bunk_entry = tk.Entry(self.entry_frame, justify=tk.RIGHT, text='Numbers of bunks to book: ')

        self.name_entry.grid(row=0, column=1, sticky='news')
        self.numb_entry.grid(row=1, column=1, sticky='news')
        self.bdes_entry.grid(row=2, column=1, sticky='news')
        self.bdat_entry.grid(row=3, column=1, sticky='news')
        self.seat_entry.grid(row=4, column=1, sticky='news')
        self.bunk_entry.grid(row=5, column=1, sticky='news')

        self.bdat_entry.delete(0, tk.END)
        self.bdat_entry.insert(0, bus.get_date_str())
        self.bdat_entry.config(state='disabled')

        self.bdes_entry.delete(0, tk.END)
        self.bdes_entry.insert(0, bus.get_destination_str())
        self.bdes_entry.config(state='disabled')

        self.seat_num_label = tk.Label(
            self.entry_frame,
            text=
            'Sorry there are no more seats available on this booking' if bus.MAX_SEAT - bus.seat == 0 else
            f'There is only 1 seat left at ${round(bus.SEAT_COST, 2)} each' if bus.MAX_SEAT - bus.seat == 1 else
            f'There are {bus.MAX_SEAT - bus.seat} seats left at ${round(bus.SEAT_COST, 2)} each')
        self.bunk_num_label = tk.Label(
            self.entry_frame,
            text=
            'Sorry there are no more bunks available on this booking' if bus.MAX_BUNK - bus.bunk == 0 else
            f'There is only 1 bunk left at ${round(bus.BUNK_COST, 2)} each' if bus.MAX_BUNK - bus.bunk == 1 else
            f'There are {bus.MAX_BUNK - bus.bunk} bunks left at ${round(bus.BUNK_COST, 2)} each')

        self.seat_num_label.grid(row=4, column=2)
        self.bunk_num_label.grid(row=5, column=2)

        self.entry_frame.columnconfigure(1, weight=1)

    def order_confirm_command(self):
        if not self.name_entry.get():
            self.name_entry.focus()
            messagebox.showerror("Error", "Name field is empty")
            return

        if not self.numb_entry.get():
            self.numb_entry.focus()
            messagebox.showerror("Error", "Phone number field is empty")
            return

        if not self.seat_entry.get().isdigit():
            self.seat_entry.focus()
            messagebox.showerror("Error", "Seat field is not an integer")
            return

        if not self.bunk_entry.get().isdigit():
            self.bunk_entry.focus()
            messagebox.showerror("Error", "Bunk field is not an integer")
            return

        booking = Booking(self.name_entry.get(), self.numb_entry.get(), int(self.seat_entry.get()), int(self.bunk_entry.get()))
        self.confirm_callback(self.bus, booking)

    def order_cancel_command(self):
        self.cancel_callback()

    def set_from_booking(self, booking: Booking):
        self.name_entry.delete(0, tk.END)
        self.numb_entry.delete(0, tk.END)
        self.seat_entry.delete(0, tk.END)
        self.bunk_entry.delete(0, tk.END)

        self.name_entry.insert(0, booking.name)
        self.numb_entry.insert(0, booking.phone_number)
        self.seat_entry.insert(0, booking.seat)
        self.bunk_entry.insert(0, booking.bunk)