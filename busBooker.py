#!/usr/bin/env python
"""
Python Version: Python 3.6.6
Author: Shannon Blackhall
Date: 29/09/2018
Program Version: V1

Simple Bus Booking System with tkinter gui
"""
import tkinter as tk
import tkinter.messagebox as messagebox
from models import Booking, Bus, State
from finishedFrame import FinishedFrame
from orderFrame import OrderFrame
from welcomeFrame import WelcomeFrame
from busFrame import BusFrame


class MainApp(tk.Frame):
    def __init__(self, parent):
        super().__init__(parent)

        self.buses = {
            State.friday: Bus(State.friday),
            State.sunday: Bus(State.sunday)
        }
        self.state = State.NULL
        self.parent = parent

        self.fridaybutton = tk.Button(self, text="Friday", command=self.friday_button_command)
        self.fridaybutton.grid(row=1, column=0, sticky="news")

        self.sundaybutton = tk.Button(self, text="Sunday", command=self.sunday_button_command)
        self.sundaybutton.grid(row=1, column=1, sticky="news")

        self.bookButton = tk.Button(self, text="Book Now!", command=self.book_button_command)
        self.bookButton.grid(row=0, column=0, sticky="news", columnspan=2)

        self.busframe = BusFrame(self)
        self.busframe.grid(row=2, column=0, columnspan=2, sticky="news")

        self.data_frame = WelcomeFrame(self)
        self.data_frame.grid(row=0, column=2, rowspan=3, sticky="news")

        self.rowconfigure(0, weight=2)
        self.rowconfigure(1, weight=1)
        self.rowconfigure(2, weight=10)

        self.columnconfigure(2, weight=1)

    def friday_button_command(self):
        self.sundaybutton.configure(relief=tk.RAISED)
        self.fridaybutton.configure(relief=tk.SUNKEN)
        self.state = State.friday
        self.busframe.set_labels(self.buses[State.friday])

    def sunday_button_command(self):
        self.fridaybutton.configure(relief=tk.RAISED)
        self.sundaybutton.configure(relief=tk.SUNKEN)
        self.state = State.sunday
        self.busframe.set_labels(self.buses[State.sunday])

    def book_button_command(self):
        self.change_to_order_frame()

    def order_cancel_button_command(self):
        if messagebox.askyesno("Print", "Are you sure\nYour entered details will be lost", icon=messagebox.WARNING):
            self.change_to_welcome_frame()

    def order_confirm_button_command(self, bus: Bus, booking: Booking):
        self.change_to_finished_frame(bus, booking)

    def finished_goback(self, bus, booking):
        self.change_to_order_frame(bus, booking)

    def finished_confirm(self, bus: Bus, booking: Booking):
        bus.booking_list.append(booking)
        self.busframe.set_labels(self.buses[self.state])
        self.change_to_welcome_frame()

    def change_to_order_frame(self, bus: Bus = None, booking: Booking = None):
        order_frame = self.data_frame
        order_frame.destroy()
        self.data_frame = OrderFrame(self, bus if bus else self.buses[self.state],
                                     self.order_confirm_button_command,
                                     self.order_cancel_button_command)
        if booking:
            self.data_frame.set_from_booking(booking)
        self.data_frame.grid(row=0, column=2, rowspan=3, sticky="news")

    def change_to_welcome_frame(self):
        order_frame = self.data_frame
        order_frame.destroy()
        self.data_frame = WelcomeFrame(self)
        self.data_frame.grid(row=0, column=2, rowspan=3, sticky="news")

    def change_to_finished_frame(self, bus: Bus, booking: Booking):
        order_frame = self.data_frame
        order_frame.destroy()
        self.data_frame = FinishedFrame(self, bus, booking, self.finished_goback, self.finished_confirm)
        self.data_frame.grid(row=0, column=2, rowspan=3, sticky="news")


def main():
    root = tk.Tk()
    root.title("Bus Booker")
    main_app = MainApp(root)
    main_app.grid(row=0, column=0, sticky="news")
    root.rowconfigure(0, weight=1)
    root.columnconfigure(0, weight=1)
    root.after(0, lambda: main_app.friday_button_command())
    root.mainloop()


if __name__ == '__main__':
    main()
